import Vue from "vue";
import App from "./App.vue";

Vue.config.productionTip = false;

new Vue({
  el: "#app",
  data: {
    counter: 0
  },
  methods: {
    increase() {
      this.counter++;
      console.log(this.counter);
    }
  },
  render: h => h(App)
}).$mount("#app");
