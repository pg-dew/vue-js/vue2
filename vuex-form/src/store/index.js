import Vue from "vue";
import Vuex from "vuex";
import user from "./user";

// https://dev.to/matheusgomes062/how-to-make-a-form-handling-with-vuex-6g0

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user
  }
});
